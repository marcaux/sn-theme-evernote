#!/bin/sh
apt update
apt install zip
zip -r sn-theme-evernote.zip ./ -x "package-lock.json" ".env" "*.sh" ".git*" ".git/*" "dist/.gitkeep" ".DS*" ".htaccess" "node_modules/*" "src/*" "*.txt"
mkdir public
mv sn-theme-evernote.zip public/
cd public
unzip sn-theme-evernote.zip
